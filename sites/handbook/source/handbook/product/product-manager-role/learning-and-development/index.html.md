---
layout: handbook-page-toc
title: Learning and Development for Product Management
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles/) - [**Processes**](/handbook/product/product-processes/) - [**Categorization**](/handbook/product/categories/) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

Welcome to Learning and Development for Product Management at GitLab! The resources here are meant to support product managers explore, learn and grow at their own pace. We aim to collect content that spans various skill levels, as well as various levels of depth/commitment. It is recommended that product managers engage with the resources here to help them have a successful journey at GitLab and in their product career as a whole. 

Most of the resources here are free but any content requiring payment [can be reimbursed following the GitLab reimbursement policies](https://about.gitlab.com/handbook/finance/expenses/#work-related-online-courses-and-professional-development-certifications).

Over time, we will add content to directly support GitLab's product management [competencies](/handbook/product/product-manager-role/#competencies), [CDF](/handbook/product/product-manager-role/#product-management-career-development-framework) and [product development flow](/handbook/product-development-flow/). We understand that the evolving product management space requires continuous learning, and GitLab is committed to providing the time needed for in-depth learning too as part of your working time. You are encouraged to ask your manager to help you carve out time for Learning and Development. 

## Recommended books and talks for all product managers

These books are highly recommended to be read by every product manager at GitLab:

- **[Marty Cagan: Inspired: How to Create Tech Products Customers Love](https://www.amazon.com/INSPIRED-Create-Tech-Products-Customers-ebook/dp/B077NRB36N)** Provides a generic overview about Product Management
     - [Related live talk by author](https://www.mindtheproduct.com/video-the-root-causes-of-product-failure-by-marty-cagan/)

- **[Melissa Perry: Escaping the Build Trap](https://www.amazon.com/Escaping-Build-Trap-Effective-Management/dp/149197379X/)** Describes how to build and operate a successful product team
     - [Related live talk by author](https://www.mindtheproduct.com/escaping-build-trap-melissa-perri/)

- **[Eric Ries: The Lean Startup](https://www.amazon.com/Lean-Startup-Entrepreneurs-Continuous-Innovation/dp/0307887898)** Discusses how to leverage efficiency to achieve optimal outcomes.
     - [Related live talk by author](https://www.youtube.com/watch?v=RSaIOCHbuYw)

### Discovery

#### User Research

##### Quick reads and videos

- [Paul Adams: Great PMs don't spend their time on solutions](https://www.intercom.com/blog/great-product-managers-dont-spend-time-on-solutions/)

##### Deeper dive

- [The Complete Guide to the Kano Model](https://foldingburritos.com/kano-model/)

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here


#### Customer Interviewing

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

- [Continuous Interviewing by Product Talk](https://learn.producttalk.org/p/continuous-interviewing)

##### Books

- [Interviewing for Research by Andrew Travers](https://trvrs.co/book/)
- [Talking to Humans](https://www.amazon.co.uk/Talking-Humans-Success-understanding-customers-ebook/dp/B00NSUEUL4)


#### Jobs to be done 

##### Quick reads and videos

- [Clayton Christensen - Understanding the Job](https://www.youtube.com/watch?v=sfGtw2C95Ms) (video 5 min)

##### Deeper dive

- [Bob Moesta & Chris Spiek - Uncovering the Jobs to be Done](https://businessofsoftware.org/2014/06/bos-2013-bob-moesta-and-chris-spiek-uncovering-the-jobs-to-be-done/) (video 57 min)
- [Tony Ulwick - Customer Centered Innovation](https://businessofsoftware.org/2015/08/tony-ulwick/) (video 57 min)
- [Xavier Russo - A step-by-step guide to using Outcome Driven Innovataion](https://medium.com/envato/a-step-by-step-guide-to-using-outcome-driven-innovation-odi-for-a-new-product-ded320f49acb)

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Clayton M. Christensen et al: Competing Against Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer-ebook/dp/B01BBPZIHM/)
- [Intercom on Jobs-to-be-Done](https://www.intercom.com/resources/books/intercom-jobs-to-be-done) (free ebook download)

#### Lean product development

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

- [Melissa Perri - Lean Product Management](https://vimeo.com/122742946) (video 42 min)

##### Online courses 

Please contribute your favorite resources here

##### Books 

Please contribute your favorite resources here

#### Growth and experimentation 

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Sean Ellis: Hacking Growth: How Today's Fastest-Growing Companies Drive Breakout Success](https://www.amazon.com/Hacking-Growth-Fastest-Growing-Companies-Breakout/dp/045149721X)
- [Intercom: The Growth Handbook](https://www.intercom.com/resources/books/growth-handbook) (free ebook download)

#### Design 

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Don Norman: The Design of Everyday Things](https://www.amazon.com/Design-Everyday-Things-Revised-Expanded/dp/0465050654)
- [Steve Krug: Don't Make Me Think](https://www.amazon.com/Dont-Make-Think-Revisited-Usability/dp/0321965515)

### Delivery

#### User stories

##### Quick reads and videos

- [How to split a user story](https://agileforall.com/how-to-split-a-user-story/)

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Jeff Patton: User Story Mapping: Discover the Whole Story, Build the Right Product](https://www.amazon.com/User-Story-Mapping-Discover-Product/dp/1491904909)

#### Backlog management

##### Quick reads and videos

- [Brandon Chu: Ruthless Prioritization](https://blackboxofpm.com/ruthless-prioritization-e4256e3520a9)

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books 

- [Donald G. Reinertsen: The Principles of Product Development Flow](https://www.amazon.com/Principles-Product-Development-Flow-Generation/dp/1935401009)

#### Working with Engineering 

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

### Business Acumen

#### Product strategy

##### Quick reads and videos

- [Marty Cagan: Vision Vs. Strategy](https://svpg.com/vision-vs-strategy/)

##### Deeper dive

- [Des Traynor: Product Strategy Revisited](https://businessofsoftware.org/2014/12/product-strategy-saying-part-2-des-traynor-bos-usa-2014/) (video 54 min)

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### Competitive analysis

##### Quick reads and videos

- [Stop Ignoring Your Competitors...](https://producthabits.com/competitor-analysis/)

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### KPIs and Metrics 

##### Quick reads and videos

- [John Doerr: Why the secret to success is setting the right goals](https://www.youtube.com/watch?v=L4N1q4RNi9I) (video 5 min)

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Christina Wodtke: Radical Focus: Achieving Your Most Important Goals with Objectives and Key Results ( OKRs )](https://www.amazon.co.uk/Radical-Focus-Achieving-Important-Objectives-ebook/dp/B01BFKJA0Y)

### Communication

#### Relationships with customers

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### Writing to inspire, align and activate 

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### Presentations, prepared and adhoc

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

### Team Management

#### Stakeholder management

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

- [Rosemary King: Stakeholders, let 'em in](https://www.mindtheproduct.com/stakeholders-building-an-open-door-culture/) (video 20 min)

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### Cross-functional team management

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### Direct team management

##### Quick reads and videos

- [The New Manager Death Spiral](https://randsinrepose.com/archives/the-new-manager-death-spiral/)

##### Deeper dive

- [The New Manager Death Spiral](https://www.youtube.com/watch?v=pAbU3WJ-NBw) (video 30 min)

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Kim Scott: Radical Candor](https://www.amazon.com/Radical-Candor-Kim-Scott/dp/B01KTIEFEE)
- [Michael Lopp: Managing Humans](https://www.amazon.com/Managing-Humans-Humorous-Software-Engineering-ebook/dp/B01J53IE1O/)

#### Remote-first collaboration

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

#### Leadership and influence

##### Quick reads and videos

Please contribute your favorite resources here

##### Deeper dive

Please contribute your favorite resources here

##### Online courses 

Please contribute your favorite resources here

##### Books

Please contribute your favorite resources here

## Socializing in Product

### Blogs, videos and podcasts and more...

There is a lot of amazing content and ongoing trends in the world of product development. Subscribing to blogs, video channels and other ongoing content streams is a great way to get inspiration on best practices and product innvoation with your team. Here are some recommendations on where to start:

#### Blogs

- https://svpg.com/articles/
- https://www.intercom.com/blog/product-and-design/
- https://producthabits.com/blog/
- https://www.mindtheproduct.com/

#### Podcasts

- https://www.mindtheproduct.com/the-product-experience/

#### Newsletters

- https://www.mindtheproduct.com/product-management-newsletter/

#### Other

- [Mind the Product Slack channel](https://www.mindtheproduct.com/product-management-slack-community/)
- [GitLab team member Viktor Nagy on Twitter](https://twitter.com/nagyviktor)


### Thought leaders and influencers

One of the best ways to stay in the know is to follow people! There are a lot of folks openly sharing their ideas and best practices. We encourage you to follow and exchange ideas with people who inspire you. Here are some recommendations on where to start:

- [Jackie Bavaro](https://twitter.com/jackiebo)
- [Marty Cagan](https://twitter.com/cagan)
- [John Cutler](https://twitter.com/johncutlefish)
- [Ken Norton](https://twitter.com/kennethn)
- [Jeff Patton](https://twitter.com/jeffpatton)
- [Roman Pichler](https://twitter.com/johncutlefish)



