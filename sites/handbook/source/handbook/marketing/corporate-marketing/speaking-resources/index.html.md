---
layout: handbook-page-toc
title: "GitLab Speakers Resources"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## For GitLab Team-members Attending Events/ Speaking

- If you are interested in finding out about speaking opportunities join the #cfp Slack channel. Deadlines for talks can be found in the Slack channel and in the master GitLab [events spreadsheet](https://docs.google.com/spreadsheets/d/16usWToIsD-loDQYpflaMiGTmERMYSieNj_QAuk5HBeY/edit#gid=1939281399).
- If you want help building out a talk, coming up with ideas for a speaking opportunity, or have a customer interested in speaking start an issue in the marketing project using the [CFP submissions template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=CFPsubmission) and tag any associated event issues. Complete as much info as possible and we will ping you with next steps. We are happy to help in anyway we can, including public speaking coaching, and building out slides.
- If there is an event you would like to attend, are attending, speaking, or have proposed a talk and you would like support from GitLab to attend this event the process goes as follows:

1. Contact your manager for approval to attend/ speak.
1. After getting approval from your manager to attend, [add your event/ talk](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/events.yml) to the [events page](/events/) and submit merge request to Emily Kyle.
1. If your travel and expenses are not covered by the conference, GitLab will cover your expenses (transportation, meals and lodging for days said event takes place). If those expenses will exceed $500, please get approval from your manager. When booking your trip, use our travel portal, book early, and spend as if it is your own money. Note: Your travel and expenses will not be approved until your event / engagement has been added to the events page.
1. If you are speaking please note your talk in the description when you add it to the Events Page.
1. If you are not already on the [speakers page](/speakers/), please [add yourself](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/#join-the-speakers-bureau).
1. We suggest bringing swag and/or stickers with you. See notes on #swag on this page for info on ordering event swag.

- If your talk is recorded, we encourage speakers to publish their talks to YouTube. Speakers should [upload their talks to GitLab Unfiltered](/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube) or, if published elsewhere on YouTube, [add the recording](https://support.google.com/youtube/answer/57792) to the `GitLab Tech Talks` playlist on GitLab Unfiltered.

- Consider using a [GitLab branded Zoom background](https://docs.google.com/presentation/d/1PM4sCuCTSmVtoCp_O-_K9BS7yrIJjh1kteMTT7PS9zI/edit#slide=id.gc6bdfa0016_0_2) during your presentation to show your GitLab pride!

## Finding and Suggesting Speakers and Submitting to CFPs

- Speakers Bureau: a catalogue of talks, speaker briefs and speakers can be found on our [Speakers Bureau page](/speakers/). Feel free to add yourself to this page and submit a MR if you want to be in our speaker portal and are interested in being considered for any upcoming speaking opportunities.
- If you have a customer interested in speaking start an issue in the marketing project using the CFP submissions template and tag any associated event issues. Complete as much info as possible and we will ping you with next steps. We are happy to help in anyway we can, including public speaking coaching.

## Best practices for public speaking

Below are some tips on being a better presenter. For an in-depth book that covers the entire speaking process, from submitting an abstract through preparing a structured talk to practicing and delivering read [Demystifying Public Speaking](https://abookapart.com/products/demystifying-public-speaking).

1. Use a problem/solution format to **tell a story**. Many talks, especially tech talks, talk about what they built first and then what the result was. Flip this around and [start with the why](https://www.ted.com/talks/simon_sinek_how_great_leaders_inspire_action). Why did you need to take the action that you did? Talking about what problems you were encountering creates a narrative tension and people will listen intently to the talk because they want to hear the solution.
1. **Drive towards an action**. Ask yourself, "What will people do once they hear this talk?" The answer can't be, "be more aware of this topic." By deciding what action you expect the audience to take you can build your talk to drive towards this action. Talks that motivate the audience to action are more engaging and memorable than talks that simply describe. Some good example answers are
    1. Contribute to an open source project.
    1. Implement the technology or process you've come up with
    1. Follow the best practices you've outlined
1. **Practice how you play**. Practicing your talk is key to being a great presenter. As much as possible, practice exactly how you plan to give the talk. Sand up and pretend you are on stage rather than sitting down. If you'll demo, build the demo first and practice the demo. Even practicing while wearing the outfit you plan to wear can help.
1. **Give concrete examples**. Real life details bring a talk to life. Examples help people to understand and internalize the concepts you present. For each of your points try to have a "for instance." As an example, "We recommend using this script to delete old logs and free up diskspace. For instance, one time our emails lit up as users were complaining about slow performance. Some were reporting tasks hanging for over an hour when they should have completed in less than a minute. It turned out we were out of diskspace because we had verbose logging enabled. Once we ran the script we saw performance return to normal levels."
1. **Be mindful of your body language** when presenting as it will impact the way the audience perceives your presentation. Move around the stage purposefully (don't pace or fidget). Make natural gestures with your hands, and maintain good posture to convey confidence and openness which will help you to better connect with your audience.

## Customer Speakers

For ideas to help customers get their submissions accepted, see [How to Get Your Presentation Accepted (video)](https://www.youtube.com/watch?v=wGDCavOCnA4) or schedule a chat with a [Developer Evangelism](/handbook/marketing/community-relations/developer-evangelism/) team member.

## GitLab Speakers Bureau
 
To join the [GitLab Speakers Bureau](/speakers/), see the [Developer Evangelist page on the Speakers Bureau](/handbook/marketing/community-relations/developer-evangelism/speakers-bureau/).
