---
layout: handbook-page-toc
title: Demand Generation Campaigns Team
description: Demand Generation Campaigns Handbook
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Goals
{: #goals .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

### Demand Generation Key Metrics
{: #key-metrics}
<!-- DO NOT CHANGE THIS ANCHOR -->

- **North Star Metric:** MQLs
- **Efficiency Metric:** Cost per MQL
- **Business Impact Metric:** CWA/Spend (closed-won attribution)
- **Supporting/Activity Metrics:** SAOs, Attributed Pipeline, Emails Sent

### MQL Targets
{: #mql-targets}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be determined

### OKRs
{: #okrs}
<!-- DO NOT CHANGE THIS ANCHOR -->

**OKRs = Objective + Key Result**

**[Overall OKRs Epic](https://gitlab.com/groups/gitlab-com/-/epics/1285)**

- **Objective:** [Implement simplified GTM motion and interlock process to increase collaboration](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1831)
- **Key Results:**
    - [Aligned FMM, ABM, Demand Gen & Corporate Marketing to GTM motions completed by end of Feb](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/747) - @jgragnola
    - [Deprecate previous use cases, campaigns, buyer’s journeys for one SSOT on GTM motions](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/2459) - @jgragnola
- **Objective:** [Launch GitLab SaaS email nurtures and marketing to Free users](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1832)
- **Key Results:**
    - [Triggered emails launched to Free.com users](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/748) - @nbsmith
    - [3 emails sent to every non opted-out Free.com user by end of Q](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/749) - @nbsmith
    - [500 MQLs attributed to Free.com user emails](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/750) - @nbsmith

### FY21-22 First Principles
{: #first-principles}
<!-- DO NOT CHANGE THIS ANCHOR -->

1. **Find new accounts** to add to the target pool (paid demand gen)
1. **Offer compelling ungated content journeys** that drive to proven web CTAs
1. **Appropriately nurture our database** (of inbound inquiries, free .com users, core users, customers)

## Team Structure
{: #team-structure .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

The Campaigns Team is aligned to sales segments with a geo breakout. Our number one focus is driving outbound MQLs, while collaborating across marketing (working closely with field marketing, sales, and SDRs in region) to develop strategies contributing to SAOs and closed won revenue against target accounts.

- **Large**
    - North America: Agnes Oetama
    - South America, APAC, Public Sector: Jenny Tiemann
    - EMEA: Eirini Pan
- **Commercial (Mid-Market and SMB)**
    - Americas: Zac Badgley
    - EMEA/APAC: Indre Kryzeviciene
- **Email Marketing**
    - Global & Lifecycle: Nout Boctor-Smith

### Meet the Team
{: #meet-the-team}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Jackie Gragnola** _Manager, Marketing Campaigns_

- **Team Prioritization**: plan prioritization of campaigns, related content and webcasts, event support, and projects for the team
- **Hiring**: organize rolling hiring plan to scale with organization growth
- **Onboarding**: create smooth and effective onboarding experience for new team members to ramp quickly and take on responsibilities on the team
- **Transition of Responsibilities**: plan for and organize efficient handoff to new team members and between team members when prioritization changes occur

**Agnes Oetama** _Sr. Marketing Campaign Manager_

- **Top-funnel campaigns for Large Segment, *North America***
  - Plan, organize, implement, and optimize top-funnel campaigns for large segment in North America
  - Analyze and share results/findings of full-funnel attribution reporting for North America Large segment
  - Organize execution schedule, timeline, and DRIs for tactics aligned to top-funnel Large North America campaign plans, including email nurture, virtual events, digital ads, etc. aimed primarily at driving Inquiries + MQLs (with consideration of SAO attribution)
- Sisense demand generation attribution reporting (working closely with Marketing Ops)
- Support North America field marketing requests (focused on mid- and bottom-funnel tactics), primarily related to Marketo, SFDC, and Pathfactory

**Jenny Tiemann** _Sr. Marketing Campaign Manager_

- **Top-funnel campaigns for Large Segment, *PubSec, APAC, and LATAM***
  - Plan, organize, implement, and optimize top-funnel campaigns for large segment in PubSec, APAC, and LATAM 
  - Analyze and share results/findings of full-funnel attribution reporting for PubSec, APAC, and LATAM Large segment
  - Organize execution schedule, timeline, and DRIs for tactics aligned to top-funnel Large PubSec, APAC, and LATAM campaign plans, including email nurture, virtual events, digital ads, etc. aimed primarily at driving Inquiries + MQLs (with consideration of SAO attribution)
- Support PubSec, APAC, and LATAM field marketing requests (focused on mid- and bottom-funnel tactics), primarily related to Marketo, SFDC, and Pathfactory

**Zac Badgley** _Sr. Marketing Campaign Manager_

- **Top-funnel campaigns for Commercial (Mid-Market and SMB) Segment, *EMEA and APAC***
  - Plan, organize, implement, and optimize top-funnel campaigns for commercial segment
  - Analyze and share results/findings of full-funnel attribution reporting for commercial segment
  - Organize execution schedule, timeline, and DRIs for tactics aligned to top-funnel commercial campaign plans, including email nurture, virtual events, digital ads, etc. aimed primarily at driving Inquiries + MQLs (with consideration of SAO attribution)
- Support email marketing initiatives with the Campaign Manager focused on email marketing

**Nout Boctor-Smith** _Sr. Marketing Campaign Manager_

-  **Lifecycle email marketing strategy**
  - Continuous Email Communicaiton: establish automated strategic email nutures in Marketo to 
  - Bi-weekly Newsletter: collaboration with Content Marketing and other teams
  - Ad hoc email requests: determine how ideas/requests fit into overall lifecycle email strategy
- **FY21 Q3 OKRs**
  - [CMO Q3'FY21 Objective: Segment our database](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1331)
  - [CMO Q3 FY21 KR Establish Continuous Email Communications (Dunk)](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1486)
  - [CMO Q3 FY21 KR - Email Nurture PLAN for Free .com Users (Dunk)](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1512)
- Collaborate with Commercial (Mid-Market and Small) Campaign Managers on segment strategy

**Eirini Panagiotopoulou** _Sr. Marketing Campaign Manager_

- **Top-funnel campaigns for Large Segment, *EMEA***
  - Plan, organize, implement, and optimize top-funnel campaigns for large segment in EMEA
  - Analyze and share results/findings of full-funnel attribution reporting for EMEA Large segment
  - Organize execution schedule, timeline, and DRIs for tactics aligned to top-funnel Large EMEA campaign plans, including email nurture, virtual events, digital ads, etc. aimed primarily at driving Inquiries + MQLs (with consideration of SAO attribution)
- Support EMEA field marketing requests (focused on mid- and bottom-funnel tactics), primarily related to Marketo, SFDC, and Pathfactory

**Indre Kryzeviciene** _Marketing Campaign Manager_

- **Top-funnel campaigns for Commercial (Mid-Market and SMB) Segment, *EMEA and APAC***
  - Plan, organize, implement, and optimize top-funnel campaigns for commercial segment
  - Analyze and share results/findings of full-funnel attribution reporting for commercial segment
  - Organize execution schedule, timeline, and DRIs for tactics aligned to top-funnel commercial campaign plans, including email nurture, virtual events, digital ads, etc. aimed primarily at driving Inquiries + MQLs (with consideration of SAO attribution)
- Support email marketing initiatives with the Campaign Manager focused on email marketing

_Each team member contributes to making day-to-day processes more efficient and effective, and will work with marketing operations as well as other relevant teams (including field marketing, content marketing, and product marketing) prior to modification of processes._

## Communication
{: #communication .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

In line with GitLab's overall [communication guidelines](https://about.gitlab.com/handbook/communication/), campaign managers work through MRs first and issues second (in order to preserve documentation), and our slack channel [#marketing_programs](https://gitlab.slack.com/archives/CCWUCP4MS) is used for team updated and quick questions.

The [#demand-gen](https://gitlab.slack.com/archives/CJFB4T7EX) channel is used for weekly check-ins and interaction with the larger demand generation team.

In between bi-weekly Campaigns Team Connect calls:
* **Updates** should be added to MRs when possible
   - Add to #marketing_programs Slack
   - Begin with `UPDATE: <brief description>` (i.e. `UPDATE: Marketo segmentation live for sales segment`)
   - Include link to MR (preferred) or issue
* **Topics** that need input should be added as an MR when possible, with issues as a secondary option, and pinged in Slack with `DISCUSS`
   - Add to #marketing_programs Slack
   - Begin with `DISCUSS: <brief description>` (i.e. `DISCUSS: Marketing tool champions`)
   - Include link to MR (preferred) or issue

### Meeting Cadence
{: #meeting-cadence}
<!-- DO NOT CHANGE THIS ANCHOR -->

Most of our team meetings are recorded and can be found [here](https://drive.google.com/drive/u/1/folders/1GDkvqVhimLDnX744eh9YS6_qHhKRafmJ).

- Bi-weekly Tuesdays - Campaign Team Connect Call (campaigns)
- Bi-weekly Wednesdays - Demand Generation Team Call (campaigns, digital, partner)
- Thursdays - "No work talk" demand gen team hangout (campaigns, digital, partner)
- Thursdays - Marketing strategy & tactics call (all marketing)

### The Handbook
{: #handbook}
<!-- DO NOT CHANGE THIS ANCHOR -->

Is our single source of truth (SSoT) for processes and relevant links

- Individual teams should link back to these SSoT sections to avoid confusion
- Collaborative tactics contain their own handbook pages
- The handbook will be iterated on as we establish and optimize processes for optimal efficiency

## Project Management
{: #project-management .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

### Turnaround Time and SLAs
{: #slas}
<!-- DO NOT CHANGE THIS ANCHOR -->

⏱ **The SLA (Service Level Agreement) - 5 business days - begins when all details (including 100% final copy) is provided in the issue.**
- Tip: Submit issues with full "Submitter Details" complete
- Tip: Bookmark a view of your issues in Blocked status ([example](https://gitlab.com/groups/gitlab-com/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-status%3A%3Ablocked&assignee_username[]=jgragnola))

**Copy must be 100% final in order to be triaged.** This includes final edits for copy, grammar, sentence structure and readability, as well as review by relevant stakeholders (such as managers, product mktg, content mktg, sales, etc.

The issue requester is responsible for ensuring that appropriate `mktg-status` labels are applied to the issue (issue templates have the tags built in to assist).

**The due date applied when the issue is submitted is not final.** The [workback calculator](https://docs.google.com/spreadsheets/d/1RvYLEUJvh9QZJZX1uXiKJPcOOSGd7dEeNneqyA2bt3A/edit#gid=969067029) is a guideline to assist your project planning, however if details are not provided in the issue 5 business days prior by requestor, the issue timeline will be at risk and likely to be pushed out.
  - If an issue due date is prior to the SLA (based on date "submitter details" were complete), the due date will be changed to meet the SLA timeline (5 BD from date of final details submitted)
  - For example, if the issue is submitted 0ct 1, with a due date of Oct 15, BUT details are not submitted until Oct 14, THEN the issue due date will be moved to Oct 21 by the triage manager.
  - The triage manager and assignee are the ONLY individuals allowed to adjust due dates upon triage.
  - 🙏 Please **do not** ask Campaign Managers to complete work in a shorter timeline as this is disruptive to their milestone, and their priority is to plan, implement, and optimize top-funnel campaigns and tactics. They will manage their milestones appropriately and pull in work as bandwidth allows, and based on the SLA.

### Issue Templates
{: #issue-templates}
<!-- DO NOT CHANGE THIS ANCHOR -->

We ask that teams request work using the following issue templates in the campaigns project. These templates more clearly indicate what information and details are required in order for the request to be triaged.

Note on timelines: The campaigns team is focused on top-funnel campaign strategy, execution, and optimization. Team member milestones include work to support top funnel goals as part of [Segment Marketing Plan](https://docs.google.com/presentation/d/1p4EmaoSb35d8ZnjKags1gUGF5T9afJW6RhBO8TR_VgA/edit#slide=id.p4) and requests will be committed to as bandwidth permits. Please see section on [turnaround time & SLAs](/handbook/marketing/demand-generation/campaigns/#slas).

#### Request Issue Templates
{: #request-issue-templates}
<!-- DO NOT CHANGE THIS ANCHOR -->

- [request-add-nurture](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-add-nurture)
- [request-confirm-target-list](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-confirm-target-list)
- [request-email-followup](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-followup)
- [request-email-invitation](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-invitation)
- [request-email-newsletter](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-newsletter)
- [request-email-reminder](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email-reminder)
- [request-email](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-email) - *only for general email requests*
- [request-mkto-landing-page](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-mkto-landing-page)
- [request-pathfactory-track](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-track)
- [request-pathfactory-upload](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload)
- [request-pathfactory-upload-bulk](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-pathfactory-upload-bulk)
- [request-program-tracking](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-program-tracking)
- [request-resource-page-addition](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-resource-page-addition)
- [request-sales-nominated](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-sales-nominated)
- [request-zoom-license-date](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-zoom-license-date)
- [request-webcast-dryrun-sched](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=request-webcast-dryrun-sched)
- [idea-campaign](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=idea-campaign)
- [idea-webcast](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=idea-webcast)

#### Campaign Team Issue Templates
{: #campaign-issues-templates}
<!-- DO NOT CHANGE THIS ANCHOR -->

The following issue templates are used by the campaigns team to organize their work, with appropriate labels applied for project management.

- [*campaign-manager-general](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=*campaign-manager-general)
  - to be used by Campaign Managers for simpler project management for their own initiated issues
  - puts straight in WIP status with pre-set for milestone, due date, and assignment
- [campaigns-brief](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-brief)
- [campaigns-landing-page-copy](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-landing-page-copy)
- [campaigns-mural](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-mural)
- [campaigns-pathfactory-track](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-pathfactory-track)
- [campaigns-pf-buyer-journey](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-pf-buyer-journey)
- [campaigns-program-tracking](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-program-tracking)
- [campaigns-sdr-enablement](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-sdr-enablement)
- [campaigns-webcast-dryrun-host](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-webcast-dryrun-host)
- [campaigns-webcast-dryrun-sched](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-webcast-dryrun-sched)
- [campaigns-webcast-prep](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-webcast-prep)
- [campaigns-webcast-live-host](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues/new?issuable_template=campaigns-webcast-live-host)

#### Fundamental principles of issue requests
{: #fundamentals}
<!-- DO NOT CHANGE THIS ANCHOR -->

The following issue templates are used by the campaigns team to organize their work, with appropriate labels applied for project management.

- `/assign @` - issues should have only DRI based on who will complete the work in the issue. If work needs to be completed by multiple people, consider breaking into separate issues with clear deliverables and due dates. (example: `/assign @jgragnola`)
- `/due 2020-` - add due date based on workback schedules (example: `/due 2020-07-13` or `/due tomorrow` or `/due 07/13/2020`)
- `/milestone %` - add milestone based on the due date of the issue (example: `/milestone %"Mktg: ` )
- `/weight ` - add weight between 1-5, with 1 being easiest (between 1-4 hours of time) and 5 being most complext (a full week of work). The majority of issues should fall into 1-2. If estimation is weight of 3+, break into separate issues for more manageable pieces to fit into milestones. (example: `/weight 1`)
- `/epic ` - add epic of the overarching campaign or project to keep things organized. In almost all cases, there is a larger initiative that the issue relates to, so it is best practice to associate issues to the most relevant epic for visibility. (example: `/epic [https://gitlab.com/groups/gitlab-com/marketing/-/epics/370](https://gitlab.com/groups/gitlab-com/marketing/-/epics/370)`)

### Campaigns Team Intake Process
{: #intake-process}
<!-- DO NOT CHANGE THIS ANCHOR -->

Beginning Q3 FY21, the demand gen campaigns team will begin an issue intake process to evaluate comprehensive details of request and assign based on milestone bandwidth and priorities. Requests may be submitted using the correct issue templates.

Issues will be triaged as demonstrated in the diagram below. Please note: All required details in a given issue template must be completed in order for the SLA timeline to begin.

<div style="width: 600px;" class="embed-thumb"> <div style="position: relative; height: 0;overflow: hidden; height: 400px; max-width: 800px; min-width: 320px; border-width: 1px; border-style: solid; border-color: #d8d8d8;"> <div style="position: absolute;top: 0;left: 0;z-index: 10; width: 600px; height: 100%;background: url(https://murally.blob.core.windows.net/thumbnails/gitlab2474/murals/gitlab2474.1598468466093-5f46b1720e54f644475491fc-61c7931a-18e2-4a73-90dd-eafcb724b939.png?v=27e40478-1cf5-43f3-8970-f6d66232a7d1) no-repeat center center; background-size: cover;"> <div style="position: absolute;top: 0;left: 0;z-index: 20;width: 100%; height: 100%;background-color: white;-webkit-filter: opacity(.4);"> </div> <a href="https://app.mural.co/t/gitlab2474/m/gitlab2474/1598468466093/3fecfe10c92a0e8b26403fe8e44305c6049185e4" target="_blank" rel="noopener noreferrer" style="transform: translate(-50%, -50%);top: 50%;left: 50%; position: absolute; z-index: 30; border: none; display: block; height: 50px; background: transparent;"> <img src="https://app.mural.co/static/images/btn-enter-mural.svg" alt="ENTER THE MURAL" width="233" height="50"> </a> </div> </div> <p style="margin-top: 10px;margin-bottom: 60px;line-height: 24px; font-size: 16px;font-family: Proxima Nova, sans-serif;font-weight: 400; color: #888888;"> You will enter this mural in View Only mode. </p></div>

#### Intake process labels
{: #intake-labels}
<!-- DO NOT CHANGE THIS ANCHOR -->

- **mktg-status::triage** - the issue will be evaluated to determine if full details are included, and ask questions as needed if the scope/details are not clear
- **mktg-status::blocked** - there were insufficient details in the issue for work to be triaged
    - The requester must provide the details and then move the status back to `mktg-status::triage` in order for the request to be reviewed again.
    - Please note: issue submission does not begin the SLA timeline; sufficient details begin the SLA timeline. [see note about turnaround time and SLAs](/handbook/marketing/demand-generation/campaigns/#slas)
- **mktg-status::wip** - the issue is assigned and moved into appropriate time-based milestone
- **mktg-status::plan** - this is used for work that is in an earlier stage of planning and a specific request has not been made
    - This will likely be used mainly by campaign managers on issues that are not high priority but there is a desire to capture the idea/notes.

#### Intake process daily/weekly steps
{: #intake-steps}
<!-- DO NOT CHANGE THIS ANCHOR -->

Team manager completes daily review of [issues in triage](https://gitlab.com/groups/gitlab-com/-/boards/1919630?&label_name%5B%5D=Marketing%20Programs) assessing the following standards:
* Are all details in the `Submitter Checklist` sufficiently complete in order to begin work?
  - if yes, check that due date is at least 5 business days from date submitter details provided, move to `mktg-status::wip`, assign based on triage diagram, and put in current milestone (to be adjusted as needed by assignee)
  - if no, comment to requester and move to `mktg-status::blocked` with reminder that SLA will begin when the details are submitted - [more details here](/handbook/marketing/demand-generation/campaigns/#slas)
* Does the request align to FY21-22 segment marketing plan?
 - if no, consider realistic timeline given priorities (and share with requester in comments, set up time to discuss if needed)

#### Project management automation
{: #project-management-automation}
<!-- DO NOT CHANGE THIS ANCHOR -->

To be built out

#### Shortcut issue comments for project management
{: #shortcut-comments}
<!-- DO NOT CHANGE THIS ANCHOR -->

**When a team member is picking up an issue from the backlog:**

```
/label ~"mktg-status::wip" 
/assign @
/milestone %Mktg:2021-
```

This will change the status to wip (work in progress), allow quick assignment and add to the appropriate milestone.

### Milestones
{: #milestones}
<!-- DO NOT CHANGE THIS ANCHOR -->

The campaigns team uses the overall 2-week marketing milestones (format of [Mktg: YYYY-MM-DD](https://gitlab.com/groups/gitlab-com/-/milestones?utf8=%E2%9C%93&search_title=mktg%3A+&state=&sort=)) to organize, prioritize, and balance campaigns, projects, and requests.

- The date in the milestone name is the END date of miltesone (Sunday)
- Note: Use of milestones was introduced to the campaigns team as of 2020-07-21.
- If issues in a milestone are not closed out by end of milestone, they are pulled to the next milestone. We will document **total issues** and **total weight** moved, to understand relative commitment levels and avoid overcommitment as we begin using milestones.

**Q3 FY21 Marketing Milestones:**

- [Mktg: 2021-02-14](https://gitlab.com/groups/gitlab-com/-/milestones/238)
- [Mktg: 2021-02-28](https://gitlab.com/groups/gitlab-com/-/milestones/239)
- [Mktg: 2021-03-14](https://gitlab.com/groups/gitlab-com/-/milestones/397)
- [Mktg: 2021-03-28](https://gitlab.com/groups/gitlab-com/-/milestones/398)
- [Mktg: 2021-04-11](https://gitlab.com/groups/gitlab-com/-/milestones/399)
- [Mktg: 2021-04-25](https://gitlab.com/groups/gitlab-com/-/milestones/400)
- [Mktg: 2021-05-09](https://gitlab.com/groups/gitlab-com/-/milestones/401)
- [Mktg: 2021-05-23](https://gitlab.com/groups/gitlab-com/-/milestones/402)
- [Mktg: 2021-06-06](https://gitlab.com/groups/gitlab-com/-/milestones/403)
- [Mktg: 2021-06-20](https://gitlab.com/groups/gitlab-com/-/milestones/404)
- [Mktg: 2021-07-04](https://gitlab.com/groups/gitlab-com/-/milestones/406)
- [Mktg: 2021-07-18](https://gitlab.com/groups/gitlab-com/-/milestones/405)

**Campaigns Team Backlog Milestones:**

- [DG-OKRs](https://gitlab.com/groups/gitlab-com/-/milestones/434) - holds issues that are KRs in OKR initiatives
- [DG-Backlog:Requests](https://gitlab.com/groups/gitlab-com/-/milestones/260) - holds issue requests to be triaged, typically related to Marketo (landing pages, emails, etc.)
- [DG-Backlog:Ideas](https://gitlab.com/groups/gitlab-com/-/milestones/258) - holds issue requests related to campaign and webcast ideas submitted to demand generation
- [DG-Backlog:Documentation](https://gitlab.com/groups/gitlab-com/-/milestones/248) - holds issues related to needed documentation (handbook, issues, etc.)
- [DG-Backlog:General](https://gitlab.com/groups/gitlab-com/-/milestones/256) - holds general issue backlog that don't fit into request, ideas, or documentation/process

### Boards and Lists
{: #boards-lists}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Boards
{: #boards}
<!-- DO NOT CHANGE THIS ANCHOR -->

<b>[Campaigns Team Current Milestone](https://gitlab.com/groups/gitlab-com/-/boards/1884920?milestone_title=%23started&&label_name[]=dg-campaigns)</b> - this board provides a high-level status view of issues in the current milestone by team member.

<b>[All Milestones Board](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&amp;utf8=%E2%9C%93&amp;state=opened&amp;label_name%5B%5D=Marketing%20Programs)</b> - this board provides high level of all milestones in consecutive order

**All Milestones by Team Member**

- [Agnes Oetama](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=aoetama)
- [Eirini Pan](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=eirinipan)
- [Indre Kryzeviciene](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=ikryzeviciene)
- [Jackie Gragnola](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=jgragnola)
- [Jenny Tiemann](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=jennyt)
- [Nout Boctor-Smith](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=nbsmith)
- [Zac Badgley](https://gitlab.com/groups/gitlab-com/-/boards/1884920?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=zbadgley)

**All Team Members by Milestone (Current Quarter)**

- [Mktg: 2020-08-30](https://gitlab.com/groups/gitlab-com/-/boards/1948068?scope=all&utf8=%E2%9C%93&milestone_title=Mktg%3A%202020-08-30)
- [Mktg: 2020-09-13](https://gitlab.com/groups/gitlab-com/-/boards/1948068?scope=all&utf8=%E2%9C%93&milestone_title=Mktg%3A%202020-09-13)
- [Mktg: 2020-09-27](https://gitlab.com/groups/gitlab-com/-/boards/1948068?scope=all&utf8=%E2%9C%93&milestone_title=Mktg%3A%202020-09-27)
- [Mktg: 2020-10-11](https://gitlab.com/groups/gitlab-com/-/boards/1948068?scope=all&utf8=%E2%9C%93&milestone_title=Mktg%3A%202020-10-11)
- [Mktg: 2020-10-25](https://gitlab.com/groups/gitlab-com/-/boards/1948068?scope=all&utf8=%E2%9C%93&milestone_title=Mktg%3A%202020-10-25)

#### Lists
{: #lists}
<!-- DO NOT CHANGE THIS ANCHOR -->

**In marketing programs project (to be sunsetted upon move to campaigns project):**

- [Issues in triage status (MPM)](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Atriage)
- [Issues in blocked status (MPM)](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Ablocked)
- [Issues in ready-to-build status (MPM)](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Aready-to-build)
- [Issues in plan status (MPM)](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Aplan)

**In campaigns project (transition of new requests to new project):**

- [Issues in triage status (campaigns)](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Atriage)
- [Issues in blocked status (campaigns)](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Ablocked)
- [Issues in ready-to-build status (campaigns)](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Aready-to-build)
- [Issues in plan status (campaigns)](https://gitlab.com/gitlab-com/marketing/demand-generation/campaigns/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=mktg-status%3A%3Aplan)

### Tips & Tricks
{: #tips-tricks}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Creating a MacBook shortcut for repetitive statements
{: #macbook-shortcuts}
<!-- DO NOT CHANGE THIS ANCHOR -->

**Example:** in an issue to update all of MPM, instead of typing out every name, I have added a shortcut in my computer to populate all of the MPMs GitLab handles when I type `asdf + Enter`.

**How to:**

- On your Mac, choose Apple menu (ever-present top left logo)
- Go to `System Preferences`
- Click the `Keyboard` section
- Click `Text` on the top nav options
- Cick the `+` at the bottom of the option list
- In `Replace` column, add the shortcut that you would type in to populate the repetitive text
- In `With` column, add the repetitive text that you want to populate when you type in the shortcut

# Reporting
{: #reporting .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

## Demand Generation Reporting
{: #demand-gen-dash .gitlab-purple}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#reporting)

### Key metrics tracked on the Demand Gen Dashboard
{: #dashboard-key-metrics}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#reporting)

### 💡 Questions that the Integrated Campaign dashboards attempt to answer
{: #dashboard-questions-to-answer}
<!-- DO NOT CHANGE THIS ANCHOR -->

#### Overall (WIP to deliver all)
{: #questions-overall}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#reporting-questions-overall)

#### By Campaign
{: #questions-by-campaign}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#questions-by-campaign)

#### Drilling down into the dashboards
{: #dashboard-drilldowns}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#dashboard-drilldowns)

## Offer-Specific Dashboards
{: #dashboard-by-offer}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#dashboard-by-tactic)

### Key Metrics tracked on ALL virtual events dashboards
{: #dashboard-virtual-events}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#dashboard-virtual-events)

#### Virtual Events Reporting
{: #reporting-virtual-events}
<!-- DO NOT CHANGE THIS ANCHOR -->

See reporting on Demand Generation Hanbook Page: [https://about.gitlab.com/handbook/marketing/demand-generation/#reporting](https://about.gitlab.com/handbook/marketing/demand-generation/#reporting-virtual-events)
